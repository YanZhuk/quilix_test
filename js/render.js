(function (window) {
	// RENDER MODULE

	function renderMessagesList(messages) {
		var messagesList = messages.reduce(function(acc, curr) {
			return acc + `
				<div class="panel panel-default">
					<div class="panel-heading">To: ${curr.to}</div>
					<div class="panel-body">
						<div>From: ${curr.from}</div>
						<div>MessageText: ${curr.messageText}</div>
						<div>MessageText: ${curr.date}</div>
					</div>
				</div>
			`
		}, '');

		$('.pup').html(messagesList);
	}

	window.renderMessagesList = renderMessagesList;



})(window);