
// Application client id
const CLIENT_ID = '1067661396251-8g382p3lpnp2lvh8cdqjfi72svb9e5eq.apps.googleusercontent.com';
// Give all access
const SCOPES = 'https://mail.google.com/';


var EMAIL = 'paulshestakov@gmail.com'
var ACCES_TOKEN = null;



function authorize() {
	gapi.load('client:auth2', function() {
		gapi.auth.authorize(
			{
				'client_id': CLIENT_ID,
				'scope': SCOPES,
				'immediate': false,
				'cookie_policy': 'single_host_origin'
			},
			function(authResult) {
				console.log(authResult);
				ACCES_TOKEN = authResult.access_token;
			}
		);
	})
}


function getDrafts() {
	return $.ajax({
		type: 'GET',
		url: 'https://www.googleapis.com/gmail/v1/users/' + EMAIL + '/drafts',
		headers: {
			Authorization: 'Bearer ' + ACCES_TOKEN
		}
	})
	.then(function(data) {
		var draftRequests =
			data.drafts.map(function(draft) {
				return $.ajax({
					type: 'GET',
					url: 'https://www.googleapis.com/gmail/v1/users/' + EMAIL + '/drafts/' + draft.id,
					headers: {
						Authorization: 'Bearer ' + ACCES_TOKEN
					}
				})
			});

		return resolveArrayOfPromises(draftRequests);
	})
	.then(function(responses) {

	  	return responses.map(function(response) {
	  		return mapMessage(response.message);
	  	})
	});
}



// Number of messages to get from server
ALL_MESSAGES_NUMBER = 20;

function getAllMail() {
	return $.ajax({
		type: 'GET',
		url: 'https://www.googleapis.com/gmail/v1/users/' + EMAIL + '/messages',
		headers: {
			Authorization: 'Bearer ' + ACCES_TOKEN
		}
	})
	.then(function(data) {
		var messagesRequests =
			data.messages
				.slice(0, ALL_MESSAGES_NUMBER)
				.map(function(message) {
					return $.ajax({
						type: 'GET',
						url: 'https://www.googleapis.com/gmail/v1/users/' + EMAIL + '/messages/' + message.id,
						headers: {
							Authorization: 'Bearer ' + ACCES_TOKEN
						}
					})
				});

		return resolveArrayOfPromises(messagesRequests);
	})
	.then(function(messages) {
		return messages.map(function(message) {
			return mapMessage(message);
		});
	});
}






function mapMessage(message) {
	var headers = message.payload.headers;

	function getHeader(headerName) {
		var header = headers.find(function(header) {
			return header.name === headerName;
		});
		if (header) {
			return header.value;
		}
		else {
			return '';
		}
	}

  	return {
		messageId: message.id,
		messageText: message.snippet,
		to: getHeader('To'),
		from: getHeader('From'),
		date: getHeader('Date'),
		subject: getHeader('Subject')
	};
}


function handleSignoutClick(event) {
	gapi.auth.signOut();
}


// UTIL:
function resolveArrayOfPromises(promises) {
	if (promises.length === 1) {
		return promises[0].then(function(data) {
			return [data];
		});
	}
	else {
		return $.when(...promises).then(function() {
			return [].slice.call(arguments, 0).map(function(data) {
				return data[0];
			});
		})
	}
}















