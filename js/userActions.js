(function(outside) {

	function getAllMessagesClicked() {
		getAllMail().then(function(messages) {
			renderMessagesList(messages);
		});
	}

	function getDraftsClicked() {
		getDrafts().then(function(messages) {
			renderMessagesList(messages);
		});
	}

	window.getAllMessagesClicked = getAllMessagesClicked;
	window.getDraftsClicked = getDraftsClicked;



})(window);